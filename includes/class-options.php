<?php

namespace Webforia_Cancel_Order;

class Options
{

    public function __construct()
    {
        add_filter('woocommerce_settings_api_form_fields_bacs', [$this, 'add_fields'], 10, 1);

    }

    /*
     * Add fields setting on woocommerce BACS options
     *
     * @hook filter woocommerce_settings_api_form_fields_bacs
     * @param [type] $fields
     * @return void
     */
    public function add_fields($fields)
    {

        $new_fields = array(
            'wco_enabled' => array(
                'title' => esc_html__('Enable/Disable', WEBFORIA_CANCEL_ORDER_DOMAIN),
                'type' => 'checkbox',
                'label' => esc_html__('Activation the automatic cancellation of orders.', WEBFORIA_CANCEL_ORDER_DOMAIN),
                'default' => 'yes',
                'description' => esc_html__('Enable this option to automatically cancel all "on Hold" orders that you have not received payment for.', WEBFORIA_CANCEL_ORDER_DOMAIN),
            ),
            'wco_days' => array(
                'title' => esc_html__('Cancel in days', WEBFORIA_CANCEL_ORDER_DOMAIN),
                'type' => 'number',
                'description' => esc_html__('Enter the number of days that the system must consider a "on Hold" order as canceled.', WEBFORIA_CANCEL_ORDER_DOMAIN),
                'default' => 3,
                'placeholder' => esc_html__('days', WEBFORIA_CANCEL_ORDER_DOMAIN),
            ),

        );

        return array_merge($fields, $new_fields);

    }
}
