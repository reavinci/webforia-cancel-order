<?php
/**
 * Plugin Name: Webforia Cancel Order
 * Description: Webforia Cancel Order memeriksa dan menganti status orderan "On Hold" menjadi "Cancelled" ketidak order belum dibayar dalam jangka waktu yang telah ditentukan.
 * Plugin URI:  https://webforia.id/webforia-cancel-order
 * Version:     1.0.0
 * Author:     Webforia Studio
 * Author URI:  https://webforia.id
 * Text Domain: webforia-cancel-order
 */

if (!defined('ABSPATH')) {
    exit;
}

define('WEBFORIA_CANCEL_ORDERTEMPLATE', plugin_dir_path(__FILE__));
define('WEBFORIA_CANCEL_ORDERASSETS', plugin_dir_url(__FILE__));
define('WEBFORIA_CANCEL_ORDERDOMAIN','webforia-cancel-order');

/*=================================================;
/* LOAD THIS PLUGIN AFTER RETHEME LOAD
/*================================================= */
function wco_plugin_loaded()
{
    // load woocommerce if woocommerce plugin active
    if (class_exists('WooCommerce')) {
        require_once WEBFORIA_CANCEL_ORDERTEMPLATE . '/plugin.php';
        require_once WEBFORIA_CANCEL_ORDERTEMPLATE . '/functions.php';
        require_once WEBFORIA_CANCEL_ORDERTEMPLATE . '/includes/include.php';
        
        // run class
        New Webforia_Cancel_Order\Plugin_Init;
        New Webforia_Cancel_Order\Options;
        New Webforia_Cancel_Order\Order;

    }

    // update plugin
    $check_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/webforia-cancel-order', __FILE__, 'webforia-cancel-order');
    $check_update->setBranch('stable_release');

    // call back after this plugin loaded
    do_action( 'wco_after_plugin_loaded');
}

add_action('rt_after_setup_theme', 'wco_plugin_loaded');